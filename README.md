# SRG API Crawler

Downloads and normalises metadata documents from the SRGSSR archives API in
order to import them into Memobase.

## Processing Steps

1. __Downloading__: The [SRGSSR archives
   endpoint](https://developer.srgssr.ch/apis/srgssr-archives/docs) provides
metadata for video and audio metadata from all business units. The crawler
fetches documents belonging to predefined set of collections of interest.
2. __Normalisation__: More in-depth data transformation is only applied later
   in the import process. The crawler however is responsible for a few basic
tasks in this domain:
   - Split the `Programme` object into individual `Item`s if such are
     available. In this case the individual `Item` is equal to the Memobase
     document, in the other case the entire `Programme`.
   - Generate a Memobase ID for each `Programme` or `Item`
3. __Forwarding__: The processed documents are sent to a Kafka topic for
   further processing.

## Configuration

The application relies on different sets of configuration:

* More or less static general application settings, e.g. the name of the Kafka
  bootstrap servers. These settings are defined via environmental variables in
  order to facilitate the reuse of already existing `ConfigMap`s in
  Kubernetes. 
* SRG API user credentials. As these values are sensitive, they should be
  provided via Kubernetes secrets;
* Collection specific metadata. This metadata is provided via Kafka messages
  (i.e. consumed by the service).
* Finally a mapping which relates Memobase collection IDs to collection names
  used in the SRG archives. The mapping is mounted as a `ConfigMap`;

### General Application Settings

* `APP_VERSION`: Application version
* `LOG_LEVEL`: Log level (`error`, `warn`, `info` or `debug`)
* `MAPPING_FILE_PATH`: Path to file containing the mapping (see below)
* `KAFKA_BOOTSTRAP_SERVERS`: Comma-separated list of Kafka bootstrap servers
* `KAFKA_INPUT_TOPIC`: Name of Kafka input topic
* `KAFKA_OUTPUT_TOPIC`: Name of Kafka output topic
* `KAFKA_REPORTS_TOPIC`: Name of Kafka reports topic
* `KAFKA_SECURITY_PROTOCOL`: Protocol used to communicate with brokers. Valid values are: `plaintext`, `ssl`, `sasl_plaintext`, `sasl_ssl`
* `KAFKA_SSL_CA_LOCATION`: File or directory path to CA certificate(s) for verifying the broker's key
* `KAFKA_SSL_KEY_LOCATION`: Path to client's private key (PEM) used for authentication
* `KAFKA_SSL_CERTIFICATE_LOCATION`: Path to client's public key (PEM) used for authentication
* `KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION`: Enable OpenSSL's builtin broker (server) certificate verification. `true` or `false`
* `KAFKA_CONSUMER_CONFIGURATIONS`: Custom Kafka consumer configurations in the
  form of `key1=value1,key2=value2,...`
* `KAFKA_PRODUCER_CONFIGURATIONS`: Custom Kafka producer configurations in the
  form of `key1=value1,key2=value2,...`
* `REPORTING_STEP_NAME`: Name of application as used in reports
* `RESULT_SIZE`: Batch size of downloaded resources from SRG API
* `CRON_JOBS`: Regular collection downloads. Leave empty or skip for no cron jobs. See below for syntax

Furthermore, the following file must be present when communicating with a protected Kafka cluster:

* Client certificate file in PEM format at the path defined in `KAFKA_SSL_CERTIFICATE_LOCATION`
* Client certificate key file in PEM format and at the path defined in `KAFKA_SSL_KEY_LOCATION`
* CA certificates in PEM format at the path defined in `KAFKA_SSL_CA_LOCATION`

#### Cron job syntax

Use the following syntax for defining collection download cron jobs:

```
<minute> <hour> <day_of_month> <month> <day_of_week> <collection> <publish_documents> <split_documents>[;<other_cron_job>]
```

Allowed for `minute`, `hour`, `day_of_month`, `month`, `day_of_week`:

* `*` (any value)
* Numerical value
* Comma-separated numerical values, e.g. `23,45`
* Range of numerical values, e.g. `23-45`
* Step values, e.g. `*/5`
* Or a combination of the above, e.g. `3-20/6,27`, meaning e.g. "each 6th minute from 3 through 20 past and 27 past"

Allowed for `collection`:

* Single Memobase collection id, e.g. `bar-001`
* Comma-separated list of Memobase collection ids, e.g. `bar-001,mav-001`
* `*` (all collections)

Allowed for `publish_documents` and `split_documents`:

* `1`: `true`
* `0`: `false`

Cronjobs are separated by semicolons


### User credentials

* `API_USER`
* `API_PASSWORD`

### Collection specific metadata

In order to initialise a download, the service consumes messages in the topic defined in `KAFKA_INPUT_TOPIC`. The messages need to have the following JSON structure:

```json
{
    "ids": ["memobase_collection_id_1", "memobase_collection_id_2"],
    "publish": true,
    "split_documents": true,
    "session_id": "abc-001-241202"
}
```

* `ids`: List of Memobase collection ids. If skipped, import every mapped collection
* `publish`: Publicly display imported documents. Defaults to `true`
* `split_documents`: Split documents in indivual items. Default to `false`
* `session_id`: Session id as used in reports. If skipped, use a timestamp (`%Y%m%d%H%M%s`)


### ID Mapping

An ID mapping  maps a Memobase collection ID to one or more SRG collections. An
SRG collection must have a `uid` as well as a `publisher` (one of `SRF`, `RTS`,
`RTR`, `RSI`, `SWI`). E.g.:

```json
{
  "srf-029": [{"uid": "urn:pdp:faro_srf:collection:1bc7149e-d791-30ce-ab56-3faef7c15316", "publisher": "SRF"}]
}
```
