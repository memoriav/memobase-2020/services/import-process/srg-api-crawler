use std::sync::Arc;

use crate::{
    api_client::ApiClient,
    document,
    mapping::{Mapping, Publisher},
    message::{self, ProcessMetadata, SendMessage},
};
use anyhow::{bail, Context, Result};
use futures::TryStreamExt;
use kafka_utils::{
    consumer::CustomConsumer,
    kafka_core::KafkaClientSettings,
    producer::{
        CustomProducer, MessageMetadataBuilder, OutputTopics, RecordMetadata, ReportMetadata,
    },
    reporting::ReportStatus,
};
use log::info;
use log::{debug, error};
use serde_json::{Map, Value};
use tokio::sync::{
    mpsc::{Receiver, Sender},
    Mutex,
};

use crate::{configuration::Configuration, message::ReportScaffold};

#[derive(Default)]
struct DocumentCounter {
    documents: usize,
    batches: usize,
}

impl DocumentCounter {
    fn new() -> Self {
        DocumentCounter::default()
    }

    fn count<T>(&mut self, docs: &[T]) {
        self.documents += docs.len();
        self.batches += 1;
    }
}

fn create_consumer_config(app_config: &Configuration) -> KafkaClientSettings<'_> {
    let mut config = Vec::new();
    config.push(format!("group.id={}", app_config.kafka_consumer_group));
    for configuration in app_config.kafka_consumer_configurations.iter() {
        config.push(configuration.clone());
    }
    create_client_config(config, app_config)
}

fn create_producer_config(app_config: &Configuration) -> KafkaClientSettings<'_> {
    let mut config = Vec::new();
    for configuration in app_config.kafka_producer_configurations.iter() {
        config.push(configuration.clone());
    }
    create_client_config(config, app_config)
}

fn create_client_config(
    mut config: Vec<String>,
    app_config: &Configuration,
) -> KafkaClientSettings<'_> {
    config.push(format!(
        "bootstrap.servers={}",
        &app_config.kafka_bootstrap_servers
    ));
    config.push(format!(
        "security.protocol={}",
        app_config.kafka_security_protocol
    ));
    config.push(format!(
        "ssl.ca.location={}",
        app_config.kafka_ssl_ca_location
    ));
    config.push(format!(
        "ssl.key.location={}",
        app_config.kafka_ssl_key_location
    ));
    config.push(format!(
        "ssl.certificate.location={}",
        app_config.kafka_ssl_certificate_location
    ));
    config.push(format!(
        "enable.ssl.certificate.verification={}",
        app_config.kafka_enable_ssl_certificate_verification
    ));
    KafkaClientSettings::from(config)
}

/// Creates a [`CustomProducer`] instance
async fn create_producer(config: &Configuration) -> Result<CustomProducer> {
    let client_config = create_producer_config(config);
    let output_topics = OutputTopics::new(
        config.kafka_output_topic.to_owned(),
        config.kafka_reports_topic.to_owned(),
    );
    CustomProducer::new(client_config, output_topics)
        .await
        .map_err(|e| e.into())
}

/// Creates a [`CustomConsumer`] instance
async fn create_consumer(config: &Configuration) -> Result<CustomConsumer> {
    let client_config = create_consumer_config(config);
    CustomConsumer::new(client_config, config.kafka_input_topic.to_owned())
        .await
        .map_err(|e| e.into())
}

pub async fn run_consumer(config: &Configuration, tx: Sender<SendMessage>) -> Result<()> {
    debug!("Starting Kafka consumer");
    let kafka_consumer = create_consumer(config)
        .await
        .context("Creating Kafka consumer failed")?;
    info!("Kafka consumer set up");

    let stream_processor = kafka_consumer.stream().try_for_each(|msg| {
        let tx = tx.clone();
        async move {
            match message::parse_incoming_kafka_message(msg) {
                Ok(collections) => tokio::spawn(async move {
                    tx.send(SendMessage::Download(collections))
                        .await
                        .expect("Couldn't send message to producer");
                }),
                Err(e) => tokio::spawn(async move {
                    tx.send(SendMessage::Error(e.to_string()))
                        .await
                        .expect("Couldn't send message to producer");
                }),
            };
            Ok(())
        }
    });
    stream_processor.await.context("Stream processing failed")?;
    Ok(())
}

pub async fn run_producer(
    mut rx: Receiver<SendMessage>,
    id_mapping: Mapping,
    config: &Configuration,
    client: Arc<Mutex<ApiClient>>,
) -> Result<()> {
    debug!("Starting Kafka producer");
    let kafka_producer = create_producer(config)
        .await
        .context("Creating Kafka consumer failed")?;
    info!("Kafka producer set up");

    let report_scaffold = ReportScaffold {
        step: config.kafka_reports_topic.clone(),
        step_version: config.reporting_step_name.clone(),
    };

    while let Some(collections) = rx.recv().await {
        match collections {
            SendMessage::Download(collections) => {
                info!("New download message from {} received", collections.source);
                let collection_metadata = collections.get_process_metadata();
                for memobase_collection_id in collections.ids.iter() {
                    if let Some(srg_collections) = id_mapping.get(memobase_collection_id) {
                        for srg_collection in srg_collections {
                            let mut locked_client = client.lock().await;
                            let process_result = process_collection(
                                &mut locked_client,
                                config.result_size,
                                &collection_metadata,
                                &report_scaffold,
                                &kafka_producer,
                                srg_collection.publisher,
                                &srg_collection.uid,
                                memobase_collection_id,
                            )
                            .await;
                            drop(locked_client);
                            if let Err(e) = process_result {
                                error!(
                            "Processing SRG collection {} for Memobase collection {} failed: {e:?}",
                            srg_collection.uid, memobase_collection_id
                        );
                                continue;
                            }
                        }
                    } else {
                        info!("No SRG ids for Memobase collection {memobase_collection_id} found");
                    }
                }
            }
            SendMessage::Error(e) => {
                fatal_setup_report(&report_scaffold, &kafka_producer, &e.to_string())
                    .await
                    .expect("Could not send fatal report");
            }
        }
    }
    Ok(())
}

async fn process_collection(
    client: &mut ApiClient,
    result_size: u16,
    process_metadata: &ProcessMetadata<'_>,
    report_scaffold: &ReportScaffold,
    kafka_producer: &CustomProducer,
    publisher: Publisher,
    srg_collection_id: &str,
    memobase_collection_id: &str,
) -> Result<()> {
    info!(
        "Start downloading SRG collection {}. Fetching first batch",
        &srg_collection_id
    );
    let mut cursor: String = "".to_string();
    let mut documents_counter = DocumentCounter::new();
    loop {
        let response = client
            .get_collection(
                Some(result_size),
                publisher,
                Some(srg_collection_id),
                if cursor.is_empty() {
                    None
                } else {
                    Some(&cursor)
                },
            )
            .await;
        if response.is_err() {
            let err_msg = format!(
                "Couldn't fetch collection {} from SRG API (part of Memobase collection {}): {}",
                srg_collection_id,
                memobase_collection_id,
                response.err().unwrap(),
            );
            error!("{}", err_msg);
            fatal_report(
                report_scaffold,
                kafka_producer,
                Some(memobase_collection_id),
                Some(process_metadata),
                &err_msg,
            )
            .await?;
            break;
        }
        let response = response.unwrap();
        debug!("Batch successfully downloaded. Parsing and splitting documents now");
        let items = if process_metadata.split_documents {
            let mut new_docs = vec![];
            for d in response.results {
                let res = document::get_items(&d);
                match res {
                    Ok(r) => {
                        new_docs.append(&mut r.to_owned());
                    }
                    Err(e) => {
                        error!("Getting items failed: {e}");
                        continue;
                    }
                }
            }
            new_docs
        } else {
            response.results
        };
        documents_counter.count(&items);
        for item in items.iter() {
            process_item(
                item,
                srg_collection_id,
                memobase_collection_id,
                report_scaffold,
                kafka_producer,
                process_metadata,
            )
            .await
            .unwrap_or_else(|e| {
                error!("Error when processing item in SRG collection {srg_collection_id}: {e:#}")
            });
        }
        if let Some(nc) = response.cursor {
            debug!(
                "More documents found for collection {}. Fetching next batch with cursor {}",
                srg_collection_id, &nc
            );
            cursor = nc.to_owned();
        } else {
            info!(
                "No more documents found for collection {}. {} documents in {} batches processed.",
                srg_collection_id, documents_counter.documents, documents_counter.batches
            );
            break;
        }
    }
    Ok(())
}

async fn process_item(
    item: &Value,
    srg_collection_id: &str,
    memobase_collection_id: &str,
    report_scaffold: &ReportScaffold,
    kafka_producer: &CustomProducer,
    process_metadata: &ProcessMetadata<'_>,
) -> Result<()> {
    match document::get_original_identifier(item) {
        Ok(ref srg_item_id) => {
            let memobase_document_id = format!("{memobase_collection_id}-{srg_item_id}");
            debug!("Processing document with new Memobase ID {memobase_document_id}");
            let document =
                document::set_memobase_identifier(item.as_object().unwrap(), &memobase_document_id)
                    .with_context(|| {
                        format!(
                    "Setting Memobase id {memobase_document_id} in document {srg_item_id} failed"
                )
                    })?;
            let sent = send_metadata(
                kafka_producer,
                &report_scaffold.step,
                memobase_collection_id,
                Some(process_metadata),
                &memobase_document_id,
                &document,
            )
            .await;
            if sent.is_err() {
                error!("Couldn't send document {memobase_document_id} to Kafka");
                fatal_report(
                    report_scaffold,
                    kafka_producer,
                    Some(memobase_collection_id),
                    Some(process_metadata),
                    "Couldn't send document to Kafka",
                )
                .await
                .with_context(|| {
                    format!("Sending report for document {memobase_document_id} failed")
                })?;
            } else {
                success_report(
                    report_scaffold,
                    kafka_producer,
                    Some(memobase_collection_id),
                    Some(process_metadata),
                    Some(&memobase_document_id),
                    "Document successfully processed",
                )
                .await
                .with_context(|| {
                    format!("Sending report for document {memobase_document_id} failed")
                })?;
                debug!("Document {} successfully processed", srg_item_id);
            };
            Ok(())
        }
        Err(err) => {
            let err_msg = format!(
                "Couldn't generate Memobase identifier from document in collection {}: {}",
                srg_collection_id, err
            );
            error!("{}", &err_msg);
            let reported = fatal_report(
                report_scaffold,
                kafka_producer,
                Some(memobase_collection_id),
                Some(process_metadata),
                &err_msg,
            )
            .await;
            if let Err(e) = reported {
                bail!(
                    "Error while sending report for document in collection {} happened: {}",
                    srg_collection_id,
                    e.to_string()
                )
            } else {
                Ok(())
            }
        }
    }
}

async fn fatal_setup_report(
    scaffold: &ReportScaffold,
    producer: &CustomProducer,
    msg: &str,
) -> Result<()> {
    fatal_report(scaffold, producer, None, None, msg).await
}

async fn fatal_report(
    scaffold: &ReportScaffold,
    producer: &CustomProducer,
    collection_id: Option<&str>,
    collection_metadata: Option<&ProcessMetadata<'_>>,
    msg: &str,
) -> Result<()> {
    send_report(
        scaffold,
        producer,
        collection_id,
        collection_metadata,
        None,
        ReportStatus::Fatal,
        msg,
    )
    .await
}

async fn success_report(
    scaffold: &ReportScaffold,
    producer: &CustomProducer,
    collection_id: Option<&str>,
    collection_metadata: Option<&ProcessMetadata<'_>>,
    record_id: Option<&str>,
    msg: &str,
) -> Result<()> {
    send_report(
        scaffold,
        producer,
        collection_id,
        collection_metadata,
        record_id,
        ReportStatus::Success,
        msg,
    )
    .await
}

async fn send_report(
    scaffold: &ReportScaffold,
    producer: &CustomProducer,
    collection_id: Option<&str>,
    collection_metadata: Option<&ProcessMetadata<'_>>,
    record_id: Option<&str>,
    report_status: ReportStatus,
    msg: &str,
) -> Result<()> {
    let report = scaffold
        .build(record_id, report_status, msg)
        .context("Creating report")?;
    let identifier = record_id.unwrap_or("unknown");
    let headers: ReportMetadata = MessageMetadataBuilder::default()
        .collection_id(collection_id)
        .is_published(collection_metadata.map(|cm| cm.publish))
        .session_id(collection_metadata.map(|cm| cm.session_id).unwrap_or(""))
        .xml_record_tag(None)
        .xml_identifier_field_name(None)
        .step_name(scaffold.step.as_str())
        .build()
        .context("Creating report metadata headers failed")?
        .into();
    producer
        .send_report(report, identifier, headers.clone(), 10000)
        .await
        .context("Sending report")
        .map(|_| ())
}

async fn send_metadata(
    producer: &CustomProducer,
    app_name: &str,
    collection_id: &str,
    collection_metadata: Option<&ProcessMetadata<'_>>,
    record_id: &str,
    msg: &Map<String, Value>,
) -> Result<()> {
    let msg = serde_json::to_string(msg).context("Serializing JSON map failed")?;
    let headers: RecordMetadata = MessageMetadataBuilder::default()
        .collection_id(collection_id)
        .is_published(collection_metadata.map(|cm| cm.publish))
        .session_id(collection_metadata.map(|cm| cm.session_id).unwrap_or(""))
        .xml_record_tag(None)
        .xml_identifier_field_name(None)
        .step_name(app_name)
        .build()
        .context("Creating message metadata headers failed")?
        .into();
    producer
        .send_metadata(&msg, record_id, headers.clone(), 10000)
        .await
        .context("Sending message")
        .map(|_| ())
}
