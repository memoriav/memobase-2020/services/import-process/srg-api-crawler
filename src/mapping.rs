use std::{
    collections::HashMap,
    fmt::Display,
    fs::File,
    io::BufReader,
    path::{Path, PathBuf},
};

use anyhow::{bail, Result};
use serde::Deserialize;
use serde_json::Value;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Mapping {
    data: HashMap<String, Vec<SrgCollection>>,
}

#[derive(Clone, Debug, PartialEq, Eq, Deserialize)]
pub struct SrgCollection {
    pub uid: String,
    pub publisher: Publisher,
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Publisher {
    Srf,
    Rsi,
    Rtr,
    Rts,
    Swi,
}

impl Display for Publisher {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Publisher::Srf => write!(f, "SRF"),
            Publisher::Rsi => write!(f, "RSI"),
            Publisher::Rtr => write!(f, "RTR"),
            Publisher::Rts => write!(f, "RTS"),
            Publisher::Swi => write!(f, "SWI"),
        }
    }
}

impl Mapping {
    pub fn new(file_path: &str, file_name: &str) -> Result<Self> {
        let json = read_in(Path::new(file_path).join(file_name))?;
        transform(json)
    }

    pub fn get(&self, memobase_collection_name: &str) -> Option<&Vec<SrgCollection>> {
        self.data.get(memobase_collection_name)
    }

    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }
}

fn read_in(file_path: PathBuf) -> Result<Value> {
    let buf_reader = BufReader::new(File::open(file_path)?);
    serde_json::from_reader(buf_reader).map_err(|e| e.into())
}

fn transform(json_value: Value) -> Result<Mapping> {
    let json = json_value.as_object();
    if json.is_none() {
        bail!(
            "JSON error. Expects object, instead got:\n{}",
            serde_json::to_string_pretty(&json_value).unwrap()
        );
    }
    let json = json.unwrap();
    let mut mapping = HashMap::new();
    for (memobase_collection_id, srg_collection_ids) in json.into_iter() {
        let srg_collection_ids = srg_collection_ids.as_array();
        if srg_collection_ids.is_none() {
            bail!(
                "JSON error: Expects array, instead got:\n{}",
                serde_json::to_string_pretty(&srg_collection_ids).unwrap()
            );
        }
        let srg_collection_ids = srg_collection_ids.unwrap();
        let srg_collection_ids: Vec<SrgCollection> = srg_collection_ids
            .iter()
            .map(|v| {
                let srg_collection: Result<SrgCollection> =
                    serde_json::from_value(v.clone()).map_err(|e| e.into());
                srg_collection
                /* v.as_object()
                .ok_or_else(|| anyhow!("SRG collection id '{v}' has not type object"))
                .map(|s| s.to_string()) */
            })
            .collect::<Result<Vec<SrgCollection>>>()?;
        //.context("JSON error: Expected string values")?;
        mapping.insert(
            memobase_collection_id.to_owned(),
            srg_collection_ids.to_owned(),
        );
    }
    Ok(Mapping { data: mapping })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn read_valid_file() {
        let mapping = r#"{
                "srf-001": [{"uid": "Echo der Zeit", "publisher": "SRF"}, {"uid": "Echo der Zeit (18:00 Uhr)", "publisher": "SRF"}],
                "srf-002": [{"uid": "Antenne", "publisher": "SRF"}, {"uid": "Antenne Spezial", "publisher": "SRF"}]
                }"#;
        let value = serde_json::from_str(mapping).unwrap();
        let mut expected = HashMap::new();
        expected.insert(
            "srf-001".to_string(),
            vec![
                SrgCollection {
                    uid: "Echo der Zeit".to_string(),
                    publisher: Publisher::Srf,
                },
                SrgCollection {
                    uid: "Echo der Zeit (18:00 Uhr)".to_string(),
                    publisher: Publisher::Srf,
                },
            ],
        );
        expected.insert(
            "srf-002".to_string(),
            vec![
                SrgCollection {
                    uid: "Antenne".to_string(),
                    publisher: Publisher::Srf,
                },
                SrgCollection {
                    uid: "Antenne Spezial".to_string(),
                    publisher: Publisher::Srf,
                },
            ],
        );
        let expected = Mapping { data: expected };
        assert_eq!(transform(value).unwrap(), expected);
    }

    #[test]
    fn read_invalid_field() {
        let mapping = r#"{
                "srf-001": ["Echo der Zeit", "Echo der Zeit (18:00 Uhr)"],
                "srf-002": ["Antenne", "Antenne Spezial", 2]
                }"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert!(transform(value).is_err());
    }

    #[test]
    fn read_invalid_setting() {
        let mapping = r#"["srf-001", "srf-002"]"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert!(transform(value).is_err());
    }

    #[test]
    fn read_invalid_collection_name_() {
        let mapping = r#"{
                "srf-001": ["Echo der Zeit", "Echo der Zeit (18:00 Uhr)"],
                "srf-002": ["Antenne", "Antenne Spezial"],
                "srf-003": "einzelner wert"
                }"#;
        let value = serde_json::from_str(mapping).unwrap();
        assert!(transform(value).is_err());
    }
}
