use anyhow::{anyhow, Result};
use chrono::Local;
pub use kafka_utils::reporting::ReportStatus;
use kafka_utils::reporting::{Report, ReportBuilder};
use rdkafka::message::{BorrowedMessage, Message};
use serde::Deserialize;

/// Helper struct for creating a new report instance
#[derive(Clone)]
pub struct ReportScaffold {
    pub step: String,
    pub step_version: String,
}

impl ReportScaffold {
    pub fn build<'a>(
        &'a self,
        id: Option<&'a str>,
        status: ReportStatus,
        message: &'a str,
    ) -> Result<Report<'a>> {
        let mut intermediary_report = ReportBuilder::default();
        intermediary_report
            .status(status)
            .message(message)
            .step(&self.step)
            .step_version(&self.step_version);
        (if let Some(i) = id {
            intermediary_report.id(i).build()
        } else {
            intermediary_report.build()
        })
        .map_err(|e| e.into())
    }
}

#[derive(Clone, Deserialize)]
pub enum SendMessage {
    Download(Download),
    Error(String),
}

/// Message model to initialise a new collection download
#[derive(Clone, Deserialize)]
pub struct Download {
    /// Memobase collections to fetch. Leave empty to download all defined
    #[serde(default)]
    pub ids: Vec<String>,
    /// Import documents but do not publish
    #[serde(default = "trueish")]
    pub publish: bool,
    /// Split documents in items if possible
    #[serde(default)]
    pub split_documents: bool,
    /// ID for unequivocally identify download session
    #[serde(default = "generate_session_id")]
    pub session_id: String,
    /// Source name
    #[serde(default = "kafka")]
    pub source: String,
}

pub struct ProcessMetadata<'a> {
    /// Publish (display) imported documents
    pub publish: bool,
    /// Split documents in items if possible
    pub split_documents: bool,
    /// ID for unequivocally identify download session
    pub session_id: &'a str,
}

impl Download {
    pub fn get_process_metadata(&self) -> ProcessMetadata {
        ProcessMetadata {
            publish: self.publish,
            split_documents: self.split_documents,
            session_id: &self.session_id,
        }
    }
}

fn generate_session_id() -> String {
    format!("{}", Local::now().naive_local().format("%Y%m%d%H%M%s"))
}

fn trueish() -> bool {
    true
}

fn kafka() -> String {
    "kafka".to_string()
}

pub fn parse_incoming_kafka_message(msg: BorrowedMessage<'_>) -> Result<Download> {
    match msg.payload().map(serde_json::from_slice) {
        Some(Ok(m)) => Ok(m),
        Some(Err(e)) => Err(anyhow!(format!("Parsing message payload failed: {e}"))),
        None => Err(anyhow!("Message contains no payload")),
    }
}
