/// Handles requests to SRG API
mod api_client;

mod configuration;
mod cron_job;
/// Metadata document handlers
mod document;
/// Helper methods for working with Kafka
mod kafka;
mod mapping;
/// Helper structs and function for report generation
mod message;

use anyhow::{bail, Context, Result};
use api_client::ApiClient;
use log::{debug, info};
use mapping::Mapping;
use message::SendMessage;
use tokio::{
    join,
    sync::{mpsc, Mutex},
};

use std::{io::Write, sync::Arc, time::Duration};

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::Builder::from_env(env_logger::Env::new().filter_or("LOG_LEVEL", "info"))
        .format(|buf, record| writeln!(buf, "{} srg-api-crawler {}", record.level(), record.args()))
        .init();
    debug!("Env logger successfully set up");

    info!("Parsing configuration");
    let config = configuration::Configuration::parse()
        .context("Configuration environment variables parsing failed")?;
    info!("Configuration values OK");
    debug!("{}", &config);

    info!("Reading mapping file from {}", &config.mapping_file_path);
    let mapping = Mapping::new(&config.mapping_file_path, &config.mapping_file_name)
        .context("Parsing of mapping file failed")?;
    if mapping.is_empty() {
        bail!(
            "No mappings defined, therefore no fetching will be possible. Stopping application..."
        )
    }

    debug!("Setting up API client");
    let client = ApiClient::build(
        Duration::from_secs(10),
        Duration::from_secs(120),
        &config.api_user,
        &config.api_password,
    )
    .await
    .context("Creating API client failed")?;
    info!("HTTP client set up");
    let client = Arc::new(Mutex::new(client));

    let (tx, rx) = mpsc::channel::<SendMessage>(100);

    let consumer_handle = kafka::run_consumer(&config, tx.clone());
    if let Some(ref cron_jobs) = config.cron_jobs {
        let runner = cron_job::prepare_cron_jobs(cron_jobs, tx.clone())?;
        tokio::spawn(async move {
            info!("Starting cron daemon");
            runner.run().await;
        });
    }
    let producer_handle = kafka::run_producer(rx, mapping, &config, client);
    let (consumer_res, producer_res) = join!(consumer_handle, producer_handle);
    consumer_res.context("Running Kafka consumer failed")?;
    producer_res.context("Running Kafka producer failed")?;
    Ok(())
}
