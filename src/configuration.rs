use anyhow::{Context, Result};

use std::{
    env::{self, VarError},
    fmt::Display,
};

#[derive(Clone, Debug)]
pub struct Configuration {
    /// Path to mapping file
    pub mapping_file_path: String,
    /// Name of mapping file
    pub mapping_file_name: String,
    /// `host:port` of Kafka brokers
    pub kafka_bootstrap_servers: String,
    /// Kafka topic name where the client is notified on collection which should be fetched
    pub kafka_input_topic: String,
    /// Kafka topic name where the processed documents are written to
    pub kafka_output_topic: String,
    /// Kafka topic name where reports are written to
    pub kafka_reports_topic: String,
    /// Protocol used to communicate with brokers. Valid values are: `plaintext`, `ssl`, `sasl_plaintext`, `sasl_ssl`
    pub kafka_security_protocol: String,
    /// File or directory path to CA certificate(s) for verifying the broker's key
    pub kafka_ssl_ca_location: String,
    /// Path to client's private key (PEM) used for authentication
    pub kafka_ssl_key_location: String,
    /// Path to client's public key (PEM) used for authentication
    pub kafka_ssl_certificate_location: String,
    /// Enable OpenSSL's builtin broker (server) certificate verification. `true` or `false`
    pub kafka_enable_ssl_certificate_verification: String,
    /// Kafka consumer group name
    pub kafka_consumer_group: String,
    /// Custom Kafka consumer settings: `key1=value1,key2=value2,...`
    pub kafka_consumer_configurations: Vec<String>,
    /// Custom Kafka producer settings: `key1=value1,key2=value2,...`
    pub kafka_producer_configurations: Vec<String>,
    /// Identifier for service in reports
    pub reporting_step_name: String,
    /// Version number of application used in reports
    pub app_version: String,
    /// API user name (used to retrieve access token)
    pub api_user: String,
    /// API user password (used to retrieve access token)
    pub api_password: String,
    /// Batch size of downloaded resources from SRG API
    pub result_size: u16,
    /// Collections which should be imported on a regular basis. Separate crons with semicolons
    pub cron_jobs: Option<Vec<String>>,
}

impl Configuration {
    /// Parse and check values from environment variables.
    pub fn parse() -> Result<Self> {
        let app_version =
            env::var("APP_VERSION").context("Missing environment variable: APP_VERSION")?;
        let mapping_file_path = env::var("MAPPING_FILE_PATH")
            .context("Missing environment variable: MAPPING_FILE_PATH")?;
        let mapping_file_name = env::var("MAPPING_FILE_NAME")
            .context("Missing environment variable: MAPPING_FILE_NAME")?;
        let kafka_bootstrap_servers = env::var("KAFKA_BOOTSTRAP_SERVERS")
            .context("Missing environment variable: KAFKA_BOOTSTRAP_SERVERS")?;
        let kafka_input_topic = env::var("KAFKA_INPUT_TOPIC")
            .context("Missing environment variable: KAFKA_INPUT_TOPIC")?;
        let kafka_output_topic = env::var("KAFKA_OUTPUT_TOPIC")
            .context("Missing environment variable: KAFKA_OUTPUT_TOPIC")?;
        let kafka_reports_topic = env::var("KAFKA_REPORTS_TOPIC")
            .context("Missing environment variable: KAFKA_REPORTS_TOPIC")?;
        let kafka_security_protocol = env::var("KAFKA_SECURITY_PROTOCOL")
            .context("Missing environment variable: KAFKA_SECURITY_PROTOCOL")?;
        let kafka_ssl_ca_location = env::var("KAFKA_SSL_CA_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_CA_LOCATION")?;
        let kafka_ssl_key_location = env::var("KAFKA_SSL_KEY_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_KEY_LOCATION")?;
        let kafka_ssl_certificate_location = env::var("KAFKA_SSL_CERTIFICATE_LOCATION")
            .context("Missing environment variable: KAFKA_SSL_CERTIFICATE_LOCATION")?;
        let kafka_enable_ssl_certificate_verification = env::var(
            "KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION",
        )
        .context("Missing environment variable: KAFKA_ENABLE_SSL_CERTIFICATE_VERIFICATION")?;
        let kafka_consumer_group = env::var("KAFKA_CONSUMER_GROUP")
            .context("Missing environment variable: KAFKA_CONSUMER_GROUP")?;
        let kafka_consumer_configurations =
            parse_kafka_client_configurations(env::var("KAFKA_CONSUMER_CONFIGURATIONS"))
                .context("Missing environment variable: KAFKA_CONSUMER_CONFIGURATIONS")?;
        let kafka_producer_configurations =
            parse_kafka_client_configurations(env::var("KAFKA_PRODUCER_CONFIGURATIONS"))
                .context("Missing environment variable: KAFKA_PRODUCER_CONFIGURATIONS")?;
        let reporting_step_name = env::var("REPORTING_STEP_NAME")
            .context("Missing environment variable: REPORTING_STEP_NAME")?;
        let api_user = env::var("API_USER")
            .map(|u| u.trim().to_string())
            .context("Missing environment variable: API_USER")?;
        let api_password = env::var("API_PASSWORD")
            .map(|p| p.trim().to_string())
            .context("Missing environment variable: API_PASSWORD")?;
        let result_size = env::var("RESULT_SIZE")
            .map_err(|e| e.into())
            .and_then(|rs| {
                rs.parse::<u16>()
                    .context(format!("Parsing of value into u16 failed: {}", rs))
            })
            .context("Missing environment variable: RESULT_SIZE")?;
        let cron_jobs = env::var("CRON_JOBS").ok().and_then(|s| {
            if s.is_empty() {
                None
            } else {
                Some(s.split(";").map(|e| e.to_string()).collect())
            }
        });
        Ok(Self {
            mapping_file_path,
            mapping_file_name,
            kafka_bootstrap_servers,
            kafka_input_topic,
            kafka_output_topic,
            kafka_reports_topic,
            kafka_security_protocol,
            kafka_ssl_ca_location,
            kafka_ssl_key_location,
            kafka_ssl_certificate_location,
            kafka_enable_ssl_certificate_verification,
            kafka_consumer_group,
            kafka_consumer_configurations,
            kafka_producer_configurations,
            reporting_step_name,
            app_version,
            api_user,
            api_password,
            result_size,
            cron_jobs,
        })
    }
}

impl Display for Configuration {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Configuration field                       value")?;
        writeln!(
            f,
            "-----------------------------------------------------------------"
        )?;
        writeln!(f, "api_password                              ***")?;
        writeln!(
            f,
            "api_user                                  {}",
            self.api_user
        )?;
        writeln!(
            f,
            "app_version                               {}",
            self.app_version
        )?;
        writeln!(
            f,
            "kafka_bootstrap_servers                   {}",
            self.kafka_bootstrap_servers
        )?;
        writeln!(
            f,
            "kafka_enable_ssl_certificate_verification {}",
            self.kafka_enable_ssl_certificate_verification
        )?;
        writeln!(
            f,
            "kafka_input_topic                         {}",
            self.kafka_input_topic
        )?;
        writeln!(
            f,
            "kafka_output_topic                        {}",
            self.kafka_output_topic
        )?;
        writeln!(
            f,
            "kafka_consumer_configurations             {}",
            self.kafka_consumer_configurations.join(",")
        )?;
        writeln!(
            f,
            "kafka_producer_configurations             {}",
            self.kafka_producer_configurations.join(",")
        )?;
        writeln!(
            f,
            "kafka_reports_topic                       {}",
            self.kafka_reports_topic
        )?;
        writeln!(
            f,
            "kafka_security_protocol                   {}",
            self.kafka_security_protocol
        )?;
        writeln!(
            f,
            "kafka_ssl_ca_location                     {}",
            self.kafka_ssl_ca_location
        )?;
        writeln!(
            f,
            "kafka_ssl_certificate_location            {}",
            self.kafka_ssl_certificate_location
        )?;
        writeln!(
            f,
            "kafka_ssl_key_location                    {}",
            self.kafka_ssl_key_location
        )?;
        writeln!(
            f,
            "mapping_file_name                         {}",
            self.mapping_file_name
        )?;
        writeln!(
            f,
            "mapping_file_path                         {}",
            self.mapping_file_path
        )?;
        writeln!(
            f,
            "reporting_step_name                       {}",
            self.reporting_step_name
        )?;
        writeln!(
            f,
            "result_size                               {}",
            self.result_size
        )
    }
}

fn parse_kafka_client_configurations(raw_value: Result<String, VarError>) -> Result<Vec<String>> {
    raw_value
        .map(|v| {
            v.split(',')
                .filter(|e| !e.is_empty())
                .map(|e| e.to_owned())
                .collect::<Vec<String>>()
        })
        .map_err(|e| e.into())
}
