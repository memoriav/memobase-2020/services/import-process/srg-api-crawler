use tokio::sync::mpsc::Sender;

use anyhow::{bail, Result};
use async_job::{async_trait, Job, Runner, Schedule};
use regex::{Captures, Regex};

use crate::message::{Download, SendMessage};

struct CollectionJob {
    cron: String,
    collections: Vec<String>,
    publish_documents: bool,
    split_documents: bool,
    tx: Sender<SendMessage>,
}

#[async_trait]
impl Job for CollectionJob {
    fn schedule(&self) -> Option<Schedule> {
        Some(format!("0 {}", self.cron).parse().unwrap())
    }
    async fn handle(&mut self) {
        let download = SendMessage::Download(Download {
            ids: self.collections.clone(),
            publish: self.publish_documents,
            split_documents: self.split_documents,
            session_id: format!(
                "{}-{}-cronjob",
                self.collections.first().unwrap_or(&"all".to_string()),
                chrono::Utc::now()
            ),
            source: "cronjob".to_string(),
        });
        self.tx
            .send(download)
            .await
            .expect("Couldn't send message to producer");
    }
}

pub fn prepare_cron_jobs(cron_jobs: &Vec<String>, tx: Sender<SendMessage>) -> Result<Runner> {
    let mut runner = Runner::new();
    for cron_job in cron_jobs {
        let (cron, collections, publish_documents, split_documents) = parse_cron(cron_job)?;
        let job = CollectionJob {
            cron,
            collections,
            publish_documents,
            split_documents,
            tx: tx.clone(),
        };
        runner = runner.add(Box::new(job))
    }
    Ok(runner)
}

fn parse_cron(cron_job: &str) -> Result<(String, Vec<String>, bool, bool)> {
    let custom_pattern = r"([-\*/0-9]+,?)+";
    let expr_pattern = r"@hourly|@daily|@weekly|@monthly|@yearly|@annually";
    let id_pattern = r"(?<all_ids>\*)|(?<multi_ids>[a-z]{3,5}-[0-9]{3},([a-z]{3,5}-[0-9]{3},?)+)|(?<single_id>[a-z]{3,5}-[0-9]{3})";

    let ids = |caps: &Captures<'_>| {
        if caps.name("all_ids").is_some() {
            vec![]
        } else if let Some(multi_ids) = caps.name("multi_ids") {
            multi_ids
                .as_str()
                .split(",")
                .map(|s| s.to_string())
                .collect()
        } else if let Some(single_id) = caps.name("single_id") {
            vec![single_id.as_str().to_owned()]
        } else {
            vec![]
        }
    };

    let expr_cron_regex = Regex::new(&format!(
        r"(?<cron>{expr_pattern}) (?<ids>{id_pattern}) (?<publish>[01]) (?<split>[01])"
    ))?;
    let custom_cron_regex = Regex::new(&format!(
        r"(?<cron>{custom_pattern} {custom_pattern} {custom_pattern} {custom_pattern} {custom_pattern}) (?<ids>{id_pattern}) (?<publish>[01]) (?<split>[01])"
    ))?;

    if let Some(caps) = expr_cron_regex.captures(cron_job) {
        Ok((
            caps["cron"].to_owned(),
            ids(&caps),
            &caps["publish"] == "1",
            &caps["split"] == "1",
        ))
    } else if let Some(caps) = custom_cron_regex.captures(cron_job) {
        Ok((
            caps["cron"].to_owned(),
            ids(&caps),
            &caps["publish"] == "1",
            &caps["split"] == "1",
        ))
    } else {
        bail!("No valid cronjob found!")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_cron_simple_expr() {
        let test_cron = "@daily abc-012 1 1";
        let res = parse_cron(test_cron).unwrap();
        let expected = (
            "@daily".to_string(),
            vec!["abc-012".to_string()],
            true,
            true,
        );
        assert_eq!(res, expected);
    }

    #[test]
    fn test_parse_cron_multi_expr() {
        let test_cron = "@daily * 1 0";
        let res = parse_cron(test_cron).unwrap();
        let expected = ("@daily".to_string(), vec![], true, false);
        assert_eq!(res, expected);
    }

    #[test]
    fn test_parse_cron_numerical_syntax() {
        let test_cron = "* * * * * abc-012 0 1";
        let res = parse_cron(test_cron).unwrap();
        let expected = (
            "* * * * *".to_string(),
            vec!["abc-012".to_string()],
            false,
            true,
        );
        assert_eq!(res, expected);
    }

    #[test]
    fn test_parse_cron_numerical_multi() {
        let test_cron = "1,2 4 */5,9,50 * * abc-012,def-345 0 0";
        let res = parse_cron(test_cron).unwrap();
        let expected = (
            "1,2 4 */5,9,50 * *".to_string(),
            vec!["abc-012".to_string(), "def-345".to_string()],
            false,
            false,
        );
        assert_eq!(res, expected);
    }

    #[test]
    fn test_parse_faulty_id_cron() {
        let test_cron = "1,2 4 */5 * * a-012,def-345 0 0";
        assert!(parse_cron(test_cron).is_err())
    }

    #[test]
    fn test_parse_faulty_syntax_cron() {
        let test_cron = "4 */5 * * abc-012,def-345 0 0";
        assert!(parse_cron(test_cron).is_err())
    }
}
