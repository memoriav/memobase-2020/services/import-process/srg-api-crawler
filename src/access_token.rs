use std::usize;

use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct AccessTokenResponse {
    pub expires_in: usize,
    pub token_type: String,
    pub access_token: String,
}
