use std::collections::HashMap;

use anyhow::{anyhow, Result};
use serde_json::{Map, Value};

/// Splits programmes object into individual items.
pub fn get_items(programme: &Value) -> Result<&Vec<Value>> {
    programme
        .as_object()
        .ok_or_else(|| anyhow!("programme is not of expected type object"))
        .and_then(|m| {
            m.get("items")
                .ok_or_else(|| anyhow!("programme contains no field `items`"))
        })
        .and_then(|m| {
            m.as_array()
                .ok_or_else(|| anyhow!("`items` field is not of expected type array"))
        })
}

/// Creates Memobase identifiers by concatenating collection ID and document ID
pub fn get_original_identifier(item: &Value) -> Result<String> {
    item.as_object()
        .ok_or_else(|| anyhow!("JSON is not of expected type object"))
        .and_then(|o| {
            o.get("identifiers")
                .ok_or_else(|| anyhow!("object contains no identifiers"))
        })
        .and_then(|o| {
            o.as_array()
                .ok_or_else(|| anyhow!("`identifiers` field is not of expected type array"))
        })
        .and_then(|arr| {
            arr.iter()
                .flat_map(|e| e.as_object())
                .find_map(|e| {
                    if let Some(t) = e.get("type") {
                        if t == "SourceId" {
                            return e.get("value");
                        }
                    }
                    None
                })
                .ok_or_else(|| anyhow!("No original identifier found"))
        })
        .and_then(|o| {
            o.as_str()
                .ok_or_else(|| anyhow!("identifier value is not of expected type string"))
        })
        .map(|s| s.to_string())
}

pub fn set_memobase_identifier(
    item: &Map<String, Value>,
    identifier: &str,
) -> Result<Map<String, Value>> {
    let mut new_map = Map::new();
    item.iter().for_each(|e| {
        if e.0 == "identifiers" {
            if let Some(v) = e.1.as_array() {
                let mut identifiers = vec![];
                v.iter().for_each(|v| identifiers.push(v));
                let mut memobase_identifier = HashMap::new();
                memobase_identifier.insert("type", "MemobaseId");
                memobase_identifier.insert("value", identifier);
                let memobase_identifier = serde_json::to_value(memobase_identifier).unwrap();
                identifiers.push(&memobase_identifier);
                let identifiers = serde_json::to_value(identifiers).unwrap();
                new_map.insert(e.0.clone(), identifiers);
            } else {
                new_map.insert(
                    "identifiers".to_string(),
                    Value::String(identifier.to_string()),
                );
            }
        } else {
            new_map.insert(e.0.to_owned(), e.1.to_owned());
        }
    });
    Ok(new_map)
}

#[cfg(test)]
mod tests {
    use super::*;
    use anyhow::Result;
    use std::{
        fs::File,
        io::{BufReader, Read},
        path::PathBuf,
    };

    fn load_resource_file(file_name: &str) -> Result<String> {
        let mut p = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        p.push("tests/resources");
        p.push(file_name);
        let f = File::open(p)?;
        let mut r = BufReader::new(f);
        let mut buf = String::new();
        r.read_to_string(&mut buf)?;
        Ok(buf)
    }

    #[test]
    fn test_get_items() -> Result<()> {
        let content = load_resource_file("programme.json")?;
        let content = serde_json::from_str(&content)?;
        let res = get_items(&content);
        assert!(res.is_ok());
        let number_of_items = res.unwrap().len();
        assert_eq!(number_of_items, 1);
        Ok(())
    }

    #[test]
    fn test_get_original_identifier() -> Result<()> {
        let content = load_resource_file("programme.json")?;
        let content: Value = serde_json::from_str(&content)?;
        let res = get_original_identifier(&content);
        assert!(res.is_ok());
        assert_eq!(
            res.unwrap(),
            String::from("7402096b-e751-49d8-af75-c42838aa216e")
        );
        Ok(())
    }

    #[test]
    fn test_set_identifier() -> Result<()> {
        let expected: Value =
            serde_json::from_str(&load_resource_file("set_identifier_expected.json")?)?;
        let content = load_resource_file("programme.json")?;
        let content: Value = serde_json::from_str(&content)?;
        let content = content.as_object().unwrap();
        let memobase_identifier = "xyz-001-7402096b-e751-49d8-af75-c42838aa216e";
        let res = set_memobase_identifier(content, memobase_identifier)?;
        let res = serde_json::to_value(res)?;
        assert_eq!(res, expected);
        Ok(())
    }
}
