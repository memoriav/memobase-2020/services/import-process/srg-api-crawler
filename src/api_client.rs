use anyhow::{anyhow, bail, Context, Result};
use log::debug;
use reqwest::{Client, Response};
use reqwest::{ClientBuilder, StatusCode};
use serde::Deserialize;
use std::time::Duration;

use crate::mapping::Publisher;

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct AccessTokenResponse {
    pub expires_in: usize,
    pub token_type: String,
    pub access_token: String,
}

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum ErrorResponse {
    LegacyErrorResponse(LegacyErrorResponse),
    FaultErrorResponse(FaultErrorResponse),
}

#[derive(Debug, Deserialize)]
#[allow(dead_code)]
pub struct LegacyErrorResponse {
    pub code: String,
    pub message: String,
    pub info: String,
}

#[derive(Debug, Deserialize)]
pub struct SuccessResponse {
    pub cursor: Option<String>,
    pub results: Vec<serde_json::Value>,
}

#[derive(Debug, Deserialize)]
pub struct FaultDetail {
    pub errorcode: String,
}

#[derive(Debug, Deserialize)]
pub struct Fault {
    pub faultstring: String,
    pub detail: FaultDetail,
}
#[derive(Debug, Deserialize)]
pub struct FaultErrorResponse {
    pub fault: Fault,
}

#[derive(Clone)]
pub struct ApiClient {
    inner: Client,
    access_token: String,
    base_url: &'static str,
    user: String,
    password: String,
}

impl ApiClient {
    pub async fn build(
        connect_timeout: Duration,
        timeout: Duration,
        user: &str,
        password: &str,
    ) -> Result<Self> {
        let client_builder = ClientBuilder::new()
            .connect_timeout(connect_timeout)
            .timeout(timeout);
        let client = client_builder
            .build()
            .context("Creating API client failed")?;
        let access_token = get_access_token(&client, user, password)
            .await
            .context("Getting access token failed")?
            .access_token;

        let base_url = "https://api.srgssr.ch/srgssr-archives/v2/archives";
        let user = user.to_string();
        let password = password.to_string();
        Ok(Self {
            inner: client,
            access_token,
            base_url,
            user,
            password,
        })
    }

    pub async fn get_collection(
        &mut self,
        limit: Option<u16>,
        publisher: Publisher,
        member_of: Option<&str>,
        cursor: Option<&str>,
    ) -> Result<SuccessResponse> {
        let mut retry = false;
        let mut response = request_collection(
            &self.inner,
            self.base_url,
            &self.access_token,
            limit,
            publisher,
            member_of,
            cursor,
        )
        .await?;
        loop {
            match response.status() {
                StatusCode::OK => {
                    let json: Result<SuccessResponse> = response.json().await.map_err(|_| {
                        anyhow!("Could not deserialise response into expected json object")
                    });
                    return json;
                }
                StatusCode::UNAUTHORIZED => {
                    bail!("No access token provided")
                }
                StatusCode::FORBIDDEN => {
                    if retry {
                        bail!("Access forbidden")
                    }
                    let new_access_token =
                        get_access_token(&self.inner, &self.user, &self.password)
                            .await
                            .context("Getting new access token failed")?
                            .access_token;
                    self.access_token = new_access_token;
                    response = request_collection(
                        &self.inner,
                        self.base_url,
                        &self.access_token,
                        limit,
                        publisher,
                        member_of,
                        cursor,
                    )
                    .await?;
                    retry = true;
                    continue;
                }
                _ => {
                    if let Ok(json) = response.json::<ErrorResponse>().await {
                        match json {
                            ErrorResponse::LegacyErrorResponse(e) => {
                                bail!("Error: {} (code: {})", e.message, e.code)
                            }
                            ErrorResponse::FaultErrorResponse(e) => bail!(
                                "Error: {} (code: {})",
                                e.fault.faultstring,
                                e.fault.detail.errorcode
                            ),
                        }
                    } else {
                        bail!("Internal server error encountered")
                    }
                }
            }
        }
    }
}

async fn request_collection(
    client: &Client,
    base_url: &str,
    access_token: &str,
    limit: Option<u16>,
    publisher: Publisher,
    member_of: Option<&str>,
    cursor: Option<&str>,
) -> Result<Response> {
    let request_url = collection_request_query(base_url, limit, publisher, member_of, cursor);
    debug!("Requesting collection with {request_url}");
    let request = client
        .get(request_url)
        .header("Authorization", &format!("Bearer {}", access_token))
        .build()?;
    client.execute(request).await.map_err(|e| e.into())
}

fn collection_request_query(
    base_url: &str,
    limit: Option<u16>,
    publisher: Publisher,
    member_of: Option<&str>,
    cursor: Option<&str>,
) -> String {
    let query = limit
        .map(|i| format!("limit={}", i))
        .into_iter()
        .chain(Some(format!("publisher={publisher}")))
        .chain(member_of.map(|m| format!("member-of={}", m)))
        .chain(cursor.map(|a| format!("cursor={}", a)))
        .fold(None, |agg, x| {
            agg.map_or_else(|| Some(format!("?{}", x)), |s| Some(format!("{}&{}", s, x)))
        });
    query.map_or_else(|| base_url.to_string(), |q| format!("{}{}", base_url, q))
}

/// Request a new access token
async fn get_access_token(
    client: &Client,
    user: &str,
    password: &str,
) -> Result<AccessTokenResponse> {
    let request = client
        .post("https://api.srgssr.ch/oauth/v1/accesstoken?grant_type=client_credentials")
        .header("Cache-Control", "no-cache")
        .header("Content-Length", "0")
        .basic_auth(user, Some(password))
        .build()
        .context("Building of request for access token failed")?;
    let response = client
        .execute(request)
        .await
        .context("Execution of request for access token failed")?;
    if !response.status().is_success() {
        bail!(
            "Request for access token failed with status {}",
            response.status().as_str()
        );
    }
    let json = response
        .json::<AccessTokenResponse>()
        .await
        .context("Parsing of access token response failed")?;
    Ok(json)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_collection_request_query_full() {
        let base_url = "https://api.srgssr.ch/srgssr-archives/v2/archives";
        let limit = Some(10);
        let publisher = Publisher::Rts;
        let member_of = Some("urn:pdp:faro_srf:collection:a306e284-b635-3190-a203-c9bccceed492");
        let cursor = Some("fawhfz0afhawofalkwaelfawfeawf");
        let res = collection_request_query(base_url, limit, publisher, member_of, cursor);
        let expected = "https://api.srgssr.ch/srgssr-archives/v2/archives?limit=10&publisher=RTS&member-of=urn:pdp:faro_srf:collection:a306e284-b635-3190-a203-c9bccceed492&cursor=fawhfz0afhawofalkwaelfawfeawf";
        assert_eq!(res, expected);
    }

    #[test]
    fn test_collection_request_query_minimal() {
        let base_url = "https://api.srgssr.ch/srgssr-archives/v2/archives";
        let limit = None;
        let publisher = Publisher::Rts;
        let member_of = None;
        let cursor = None;
        let res = collection_request_query(base_url, limit, publisher, member_of, cursor);
        let expected = "https://api.srgssr.ch/srgssr-archives/v2/archives?publisher=RTS";
        assert_eq!(res, expected);
    }

    #[test]
    fn test_error_message_body_parsing() {
        let test_str = r#"{
  "fault": {
    "faultstring": "Spike arrest violation. Allowed rate : MessageRate{messagesPerPeriod=900, periodInMicroseconds=60000000, maxBurstMessageCount=90.0}",
    "detail": {
      "errorcode": "policies.ratelimit.SpikeArrestViolation"
    }
  }
}"#;
        let as_json: Result<ErrorResponse> = serde_json::from_str(test_str).map_err(|e| e.into());
        assert!(as_json.is_ok());
    }
}
