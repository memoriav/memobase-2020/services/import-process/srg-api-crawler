FROM debian:bookworm-slim
ENTRYPOINT ["/app/app"]
WORKDIR /app
RUN apt-get update && apt-get install libssl3
COPY target/app app
